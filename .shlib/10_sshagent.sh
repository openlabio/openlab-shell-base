# sshagent.sh - Handle SSH agent setup for a shell.

# This function also takes into account the situation in which agent
# forwarding is active.
sshagent() {

    # Check the current environment for a functional SSH Agent socket.
    if _ssh_agent_check_sock ; then
        return 0
    fi

    # Create ~/.ssh with proper permissions if necessary.
    if [ ! -d ~/.ssh ] ; then
        mkdir ~/.ssh && chmod 700 ~/.ssh
	if [ $? -ne 0 ] ; then
            return 1
        fi
    fi
 
    # If no identity file exists under .ssh, starting an agent is pointless.
    grep 'PRIVATE KEY' ~/.ssh/* 2>&1 >/dev/null
    if [ $? -ne 0 ] ; then
        return 1
    fi

    _ssh_agent_env_file="$(echo ~/.ssh/agent.env)"

    # If an agent file exists, validate it. If valid, return. If not, fork
    # a new agent.
    if _ssh_agent_check_env_file ${_ssh_agent_env_file} ; then
        unset -v _ssh_agent_env_file
        return 0
    else
        command -p ssh-agent > ${_ssh_agent_env_file}
        . ${_ssh_agent_env_file} 2>&1 >/dev/null 
        unset -v _ssh_agent_env_file
    fi
}

# Check the agent for correct functioning.
_ssh_agent_check_sock() {
    if [ -S ${1} ] ; then
        command -p ssh-add -l >/dev/null 2>&1
        if [ $? -lt 2 ] ; then
            # A return value < 2 indicates trouble with a running agent (e.g.
            # no identities). A return value of 2 (according to OpenSSH source
            # code) indicates a failure connecting to the agent.
            return 0
        fi
    fi
    return 1
} 2>&1 >/dev/null

# Check the env file
_ssh_agent_check_env_file() {
    if [ ! -r ${1} ] ; then
        return 1
    fi
    . ${1} && _ssh_agent_check_sock
    if [ $? -ne 0 ] ; then
        rm ${1}
        return 1
    fi
    return 0
} 2>&1 >/dev/null

_cleanup() {
    unset -f ssh_agent _ssh_agent_check_sock _ssh_agent_check_env_file _cleanup
}
