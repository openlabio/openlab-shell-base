(if (or (not (getenv "CYGWIN_HOME")) (not (server-running-p)))
    (server-start))

;; Set up packages
(require 'package)
(add-to-list 'package-archives
             '("melpa-stable" . "https://stable.melpa.org/packages/"))
(setq package-list '(exec-path-from-shell powerline json-mode
                                          json-reformat magit
                                          markdown-mode yaml-mode
                                          solarized-theme))
(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))
(dolist (package package-list)
  (unless (package-installed-p package)
    (package-install package)))

;; User interface customization.
(if (fboundp 'tool-bar-mode)
    (tool-bar-mode -1))
(if (fboundp 'scroll-bar-mode)
    (scroll-bar-mode -1))
(if (display-graphic-p)
    (progn
      (when (memq window-system '(mac ns))
        (exec-path-from-shell-initialize))
      (if (package-installed-p 'solarized-theme)
          (load-theme 'solarized-dark t))
      (global-hl-line-mode 1))
  (progn
    (if (fboundp 'menu-bar-mode)
        (menu-bar-mode -1))))


(when (package-installed-p 'powerline)
  (progn
    (require 'powerline)
    (powerline-vim-theme)))

(global-font-lock-mode 1)
(column-number-mode 1)
(setq linum-format "%4d \u2502 ")
(global-linum-mode 1)
(setq inhibit-startup-message t)
(setq inhibit-splash-screen t)
(setq initial-scratch-message nil)

;; y for yes, n for no
(defalias 'yes-or-no-p 'y-or-n-p)

;; Turn off backup files
(setq make-backup-files nil)

;; Use UTF-8 by default
(prefer-coding-system 'utf-8)
(set-language-environment "UTF-8")

;; Magit configuration
(if (package-installed-p 'magit)
    (progn
      (global-set-key (kbd "C-x g") 'magit-status)))

;; Tab always inserts 4 spaces
(setq-default indent-tabs-mode nil)
(setq tab-width 4)

;; Mode configurations
(add-hook 'emacs-lisp-mode-hook 'turn-on-eldoc-mode)

;; Whitespace mode
(setq whitespace-style
      (quote (face trailing tabs spaces lines-tail
                   newline space-mark tab-mark newline-mark)))
(setq whitespace-line-column 72)
(setq whitespace-display-mappings
      '(
        (space-mark 32 [183] [46])
        (newline-mark 10 [182 10])
        (tab-mark 9 [8677 9] [92 9])
        ))

;; JSON mode
(add-hook 'json-mode-hook
          '(lambda ()
             (setq js-indent-level 2)
             (setq json-reformat:indent-width 2)
             (setq tab-width 2)))

;; YAML mode configuration
(if (package-installed-p 'yaml-mode)
    (progn
      (require 'yaml-mode)
      (add-to-list 'auto-mode-alist '("\\.yml\\'" . yaml-mode))))
(add-hook 'yaml-mode-hook
          '(lambda ()
             (define-key yaml-mode-map "\C-m" 'newline-and-indent)))


;; TODO review
(setq european-calendar-style 't)
(setq calendar-week-start-day 1)
(setq ps-paper-type 'a4)
