# tools - Set up the runtime environment for various tools

tools() {
    tools_dir="${HOME}/tools"
    if [ ! -d "${tools_dir}" ] ; then
	return 0
    fi

    # Most recent version of Apache ant
    try_ant_home="$(ls -dr ${tools_dir}/apache-ant-* 2>/dev/null | head -1)"
    if [ -x "${try_ant_home}/bin/ant" ] ; then
	ANT_HOME="${try_ant_home}"
	PATH="${ANT_HOME}/bin:${PATH}"
	export ANT_HOME
	export PATH
    fi
    unset -v try_ant_home
    
    # Most recent version of Apache maven
    try_mvn_home="$(ls -dr ${tools_dir}/apache-maven-* 2>/dev/null | head -1)"
    if [ -x "${try_mvn_home}/bin/mvn" ] ; then
	PATH="${try_mvn_home}/bin:${PATH}"
	export PATH
    fi
    unset -v try_mvn_home

    # Packer installation
    try_packer_dir="${tools_dir}/packer"
    if [ -d "${try_packer_dir}" ] && [ -x "${try_packer_dir}/packer" ]; then
        PATH="${try_packer_dir}:${PATH}"
        export PATH
    fi
    unset -v try_packer_dir
    
    # If an Emacs application is installed...
    app_emacs="/Applications/Emacs.app/Contents/MacOS/bin"
    if [ -d "${app_emacs}" ] ; then
        PATH="${app_emacs}:${PATH}"
        export PATH
    fi
    unset -v app_emacs

    # /usr/local/bin
    if [ -d /usr/local/bin ] ; then
        PATH="/usr/local/bin:${PATH}"
        export PATH
    fi
}

_cleanup() {
    unset -f tools _cleanup
}
