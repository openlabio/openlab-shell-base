# .bash_profile - Bash specific login shell configuration

# Source generic profile.
. "${HOME}/.profile"

# Source .bashrc
. "${HOME}/.bashrc"

