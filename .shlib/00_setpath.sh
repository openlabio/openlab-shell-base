
# setpath - Set the path depending on the current operating system
setpath() {
    os="$(uname -s)"
    if [ -z "${os}" ] ; then
        return 0
    else
        posixpath ${os}
        babun ${os}
        darwin ${os}
    fi
}

# posixpath - Set the PATH to something POSIX compliant (SUSv3 preferred)
#  $1: the current operating system implementation
posixpath() {

    if [ -z "${1}" ] ; then
        return 1
    else
        os=${1}
    fi

    # On Solaris, the location of the getconf utility determines the standard
    # used:
    #    * /usr/xpg6/bin/getconf PATH:
    #        POSIX.1 (SUSv3; IEEE1003.1-1990) compliant PATH
    #    * /usr/xpg4/bin/getconf PATH:
    #        SUSv2, POSIX2, POSIX 2.a, XPG4 compliant PATH
    if [ "${os}" = "SunOS" ] ; then
        if [ -x /usr/xpg6/bin/getconf ] ; then
            pa="$(/usr/xpg6/bin/getconf PATH)"
        else
            if [ -x /usr/xpg4/bin/getconf ] ; then
                pa="$(/usr/xpg4/bin/getconf PATH)"
            fi
        fi
        # Also on Solaris, add /usr/sbin, as some common utilities are there
        pa="${pa}:/usr/sbin"
    fi
    # Assume that getconf PATH gives you something compliant on other platforms
    if [ -z "${pa}" ] ; then
        pa="$(command -p getconf PATH)"
    fi
    if [ ! -z "${pa}" ] ; then
        PATH="${pa}"
        export PATH
    fi
    unset -v pa
}

# babun - Babun specific PATH customizations
#  $1: the current operating system implementation (ignored)
babun() {
    if [ -z "${BABUN_HOME}" ] ; then
	return 0
    fi
    if [ -x /usr/local/bin/pact ] ; then
	PATH="/usr/local/bin:${PATH}"
	export PATH
    fi
}

# darwin - Mac OSX specific customizations
#  $1: the current operating system implementation
darwin() {

    if [ -z ${1} ] ; then
        return 1
    else
        os=${1}
    fi

    if [ "${os}" != "Darwin" ] ; then
	return 0
    fi

    # Test for the existence of homebrew
    if [ -x /usr/local/bin/brew ] ; then
	PATH="/usr/local/bin:${PATH}"
	export PATH
    fi

    # Set the JAVA_HOME to the most recent JVM available
    if [ -x /usr/libexec/java_home ] ; then
	JAVA_HOME="$(/usr/libexec/java_home)"
	PATH="${JAVA_HOME}/bin:${PATH}"
	export PATH
	export JAVA_HOME
    fi

}

_cleanup() {
    unset -v os
    unset -f setpath posixpath babun darwin _cleanup
}
