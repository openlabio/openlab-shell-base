# .profile - Contains generic login shell customizations.
#
# Usage by shell:
#     * bash: ignores this file if .bash_profile or .bash_login exists.
#     * bash (as sh): sources this file.
#     * ksh: sources this file.
#
# POSIX shells (including bash invoked in posix mode), ignore this file and
# expand $ENV. The result of this is sourced if euid == ruid and egid == rgid.

# Set the ENV variable
ENV="${HOME}/.shrc"
export ENV

