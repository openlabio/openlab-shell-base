# Home Directory repository

This repository contains a home directory layout suitable for use on
UNIX-alike systems.

All relative paths in the remainder of this document are rooted at `~`.

## Shell initialization files

Shell initialization files are invoked differently by different shells.

### Bash invoked as bash

If the shell is a login shell (interactive or not), `/etc/profile` is
sourced, and then `.bash_profile`, `.bash_login` or `.profile` (but
only the first one of the three that is readable).

If the shell is an interactive non-login shell, only `.bashrc` is sourced.

### Bash invoked as sh

When invoked as `sh`, and if the shell is a login shell (interactive or
not), `/etc/profile` is sourced and than `.profile`.

When invoked as `sh` and when the shell is not a login shell, `$ENV` is
expanded and the resulting file name is sourced.

### Bash invoked in posix mode

When invoked in posix mode, the variable `$ENV` is expanded and the
resulting filename is sourced.

### POSIX shell

An interactive POSIX compliant shell expands the `$ENV` variable and
sources the result when effective user/group id matches the real
user/group id.

### ksh

When the shell is a login shell, ksh sources `/etc/profile`, then
`~/.profile`, then the `.profile` file in the current directory, and
finally it expands `$ENV` and sources the resulting file name.

An interactive ksh shell sources `$ENV`.

### Customization approach

To enable a consistent working environment throughout multiple shells,
all customizations are done in a file called `.shrc`.

That file is independent of a particular shell. As we do away with the
distinction between login or non-login shells, and with the
distinction between interactive and non-interactive shells, there are
a few rules to adhere to when writing customizations:

1. Customizations must not produce output
2. Customizations must not assume they are run only once.
3. Customizations must check whether the shell is interactive or not.

The role of `.profile` is limited to setting `$ENV` to `.shrc`.

The role of `.bash_profile` is to source `.profile` and `.bashrc`.

The role of `.bashrc` is to source `.shrc` and adapt `PS1` to use `bash`
optimizations.
