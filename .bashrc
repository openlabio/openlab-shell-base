# .bashrc - Bash specific interactive shell customizations.

# Source generic customization
. ~/.shrc

# Set Bash specific options

# Use bash specific PS1 construct.
PS1="\[\e[1m\]\u@\h\[\e[0m\]:\W\$ "
export PS1
