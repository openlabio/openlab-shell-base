#!/bin/sh

# This script installs dotfiles in the user directory of the current user.
#
# Quite a number of alternatives seem to exist, but all of them depend
# either on rsync to be installed (not guaranteed), or on operating
# with symbolic links (undesirable for various reasons).

source_base="$(pwd)"
target_base="${HOME}"

if [ ! -d "${target_base}" ] ; then
  exit 1
fi

for dir in .shlib .emacs.d ; do
    cp -r ${dir} $target_base 
done

for file in .bash_profile .bashrc .profile .shrc ; do
  source="${source_base}/${file}"
  target="${target_base}/${file}"

  if [ -f "${source}" -a -r "${source}" ] ; then
    cp "${source}" "${target}"
    if [ $? -ne 0 ] ; then
      echo Bwerk!
    fi
  fi
done

